// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

@custom:security-contact info@foil.network
contract FoilNetworkNFTGamesBlockchain is ERC20, Ownable, ERC20Burnable {

   // use 'message' for command to external services
   function transfer(address recipient, uint256 amount, string message) external returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    function transferFrom(address sender, address recipient, uint256 amount, string message) external returns (bool) {
        return transferFrom(sender, recipient, amount);
    }

    constructor() ERC20("Foil.network - NFT & Games blockchain", "FOIL") {
        _mint(msg.sender, 100000000 * 10 ** decimals());
    }

}
