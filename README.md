# FOIL Token
The FOIL token is governance token in FOILNetwork Platform.
The FOIL token is inherited from the standard of ERC20 in Ethereum (and same) protocol.

see https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol



Total Supply : 100 M
Name: FOIL
Symbol:FOIL
Solidity version: 0.8.6

ERc-20m - `message` parameter added to `transfer` and `transferFrom` functions




